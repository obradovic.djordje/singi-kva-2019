import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http'; 
import { Observable, of } from 'rxjs';

import {Student} from './student';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}; 


@Injectable({
  providedIn: 'root'
})
export class StudentServiceService {

  constructor(private http: HttpClient) {}
  

  addStudent(student:Student){
    return this.http.put("http://localhost:8080/students/", student, httpOptions);
  }


  getStudents():Observable<Student[]> {
    return this.http.get<Student[]>('http://localhost:8080/students/');
  }

}
