import { Component } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http'; 
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';


import {StudentServiceService} from './student-service.service';
import {Student} from './student';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'jpaDemo';
  //http:HttpClient;

  constructor(private service:StudentServiceService, private http: HttpClient ){}

  listaStudenata:Student[];

  preuzmiStudente():void{

    let rez = this.service.getStudents();
    //let rez:Observable<Student[]> = this.http.get<Student[]>('http://localhost:8080/students/');

    rez.subscribe(X=>this.listaStudenata = X);
    console.log('1')
    console.log(rez);

  }


  student:Student = new Student();

  addStudent(){
    this.service.addStudent(this.student).subscribe(X=>this.preuzmiStudente());
  }

  removeStudent(student:Student){
    this.http.delete('http://localhost:8080/students/'+student.id).subscribe(X=>this.preuzmiStudente());
  }


}
