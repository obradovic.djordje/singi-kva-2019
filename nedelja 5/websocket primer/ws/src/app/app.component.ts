import { Component } from '@angular/core';

import { HttpClient} from '@angular/common/http';
import { webSocket } from 'rxjs/webSocket' // for RxJS 6, for v5 use Observable.webSocket


export class Message {

  constructor(
      public sender: string,
      public content: string,
      public isBroadcast = false,
  ) { }
}


export class AppMessage{

  public sender:String;
  public receiver:String;
  public body:String;

  constructor(){}
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ws';

  private wsclient;
  private msg = "";
  private lista:AppMessage[] = [];


  constructor(private http: HttpClient) {
    this.wsclient = webSocket('ws://localhost:8080/websocket');
    this.wsclient.subscribe(
       (msg)=>{
         this.lista.push(msg);
        },
       (err) => console.log(err),
       () => console.log('complete')
     );
  }


    send(){
      let msg:AppMessage = new AppMessage();
      msg.body = this.msg;
      msg.sender = 'sender';
      msg.receiver = 'receiver';
      this.wsclient.next(JSON.stringify(msg));
      console.log('tu sam');
    }

    getTest(){
      this.http.get<String>("http://localhost:8080/test").subscribe(
        (res)=>{
          console.log(res);
        }
      )
    }


}
